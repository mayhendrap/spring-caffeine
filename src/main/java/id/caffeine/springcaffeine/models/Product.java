package id.caffeine.springcaffeine.models;

import java.math.BigDecimal;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;

@Data
@Entity
@Table(name = "products")
public class Product {

  @Id
  private Integer id;

  private String name;

  private BigDecimal price;

}
