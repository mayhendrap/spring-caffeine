package id.caffeine.springcaffeine.mappers.response;

import id.caffeine.springcaffeine.models.Product;
import java.util.List;
import lombok.Value;

@Value(staticConstructor = "of")
public class ProductResponse {
  List<Product> products;
}
