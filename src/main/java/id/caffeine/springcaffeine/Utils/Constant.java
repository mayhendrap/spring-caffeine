package id.caffeine.springcaffeine.Utils;

public class Constant {
  
  private Constant() {}
  
  public static String PRODUCT_CACHE_NAME = "getProducts";
  public static String PRODUCT_CACHE_KEY = "products";
  
}
