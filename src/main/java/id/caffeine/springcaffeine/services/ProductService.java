package id.caffeine.springcaffeine.services;

import id.caffeine.springcaffeine.mappers.response.ProductResponse;

public interface ProductService {

  ProductResponse getProducts();

}
