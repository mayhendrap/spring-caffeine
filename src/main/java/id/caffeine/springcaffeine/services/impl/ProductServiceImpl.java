package id.caffeine.springcaffeine.services.impl;

import org.springframework.cache.CacheManager;
import org.springframework.stereotype.Service;
import id.caffeine.springcaffeine.Utils.Constant;
import id.caffeine.springcaffeine.caches.ProductCache;
import id.caffeine.springcaffeine.mappers.response.ProductResponse;
import id.caffeine.springcaffeine.services.ProductService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RequiredArgsConstructor
@Service
public class ProductServiceImpl implements ProductService {
  
  private final CacheManager cacheManager;
  private final ProductCache productCache;

  @Override
  public ProductResponse getProducts() {
    if (cacheManager.getCache(Constant.PRODUCT_CACHE_NAME) != null && cacheManager
        .getCache(Constant.PRODUCT_CACHE_NAME).get(Constant.PRODUCT_CACHE_KEY) != null) {
      ProductResponse cachedProduct = cacheManager.getCache(Constant.PRODUCT_CACHE_NAME)
          .get(Constant.PRODUCT_CACHE_KEY, ProductResponse.class);
      log.info("CACHED RESPONSE : {}", cachedProduct);
      return cachedProduct;
    }

    productCache.cachingProducts();
    return getProducts();
  }

}
