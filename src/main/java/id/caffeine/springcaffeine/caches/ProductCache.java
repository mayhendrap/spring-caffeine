package id.caffeine.springcaffeine.caches;

import java.util.List;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;
import id.caffeine.springcaffeine.Utils.Constant;
import id.caffeine.springcaffeine.mappers.response.ProductResponse;
import id.caffeine.springcaffeine.models.Product;
import id.caffeine.springcaffeine.repositories.ProductRepository;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Component
public class ProductCache {

  private final ProductRepository productRepository;
  private final CacheManager cacheManager;

  @Cacheable(cacheNames = "getProducts")
  public void cachingProducts() {
    List<Product> products = productRepository.findAll();
    cacheManager.getCache(Constant.PRODUCT_CACHE_NAME).put(Constant.PRODUCT_CACHE_KEY,
        ProductResponse.of(products));
  }

}
