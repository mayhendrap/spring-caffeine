package id.caffeine.springcaffeine.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import id.caffeine.springcaffeine.mappers.response.ProductResponse;
import id.caffeine.springcaffeine.services.ProductService;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@RestController
@RequestMapping(path = "/products")
public class ProductController {

  private final ProductService productService;

  @GetMapping
  public ProductResponse getProducts() {
    return productService.getProducts();
  }

}
